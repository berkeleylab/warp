DEBUG = #-g --farg "-O0"
FARGS = #--farg "-I/usr/local/mpi/include"
FCOMP =
FCOMPEXEC = --fcompexec mpifort
SO = so
VERBOSE = #-v
FORTHON = Forthon3
PYTHON = python3
BUILDBASEDIR = build3
INSTALLOPTIONS = #--user
-include Makefile.local3.pympi
BUILDBASE = --build-base $(BUILDBASEDIR)
TEMPBUILDDIR = $(BUILDBASEDIR)/temp_parallel
INSTALLARGS = --pkgbase warp $(BUILDBASE)
MPIPARALLEL = --farg "-DMPIPARALLEL"

install: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/envparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep $(TEMPBUILDDIR)/f3dparallelpydep $(TEMPBUILDDIR)/wxyparallelpydep $(TEMPBUILDDIR)/fxyparallelpydep $(TEMPBUILDDIR)/wrzparallelpydep $(TEMPBUILDDIR)/frzparallelpydep $(TEMPBUILDDIR)/herparallelpydep $(TEMPBUILDDIR)/cirparallelpydep $(TEMPBUILDDIR)/choparallelpydep $(TEMPBUILDDIR)/em3dparallelpydep ranffortran.c
	$(PYTHON) setup.py $(FCOMP) $(FCOMPEXEC) --parallel build $(BUILDBASE) install $(INSTALLOPTIONS)

build: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/envparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep $(TEMPBUILDDIR)/f3dparallelpydep $(TEMPBUILDDIR)/wxyparallelpydep $(TEMPBUILDDIR)/fxyparallelpydep $(TEMPBUILDDIR)/wrzparallelpydep $(TEMPBUILDDIR)/frzparallelpydep $(TEMPBUILDDIR)/herparallelpydep $(TEMPBUILDDIR)/cirparallelpydep $(TEMPBUILDDIR)/choparallelpydep $(TEMPBUILDDIR)/em3dparallelpydep ranffortran.c
	$(PYTHON) setup.py $(FCOMP) $(FCOMPEXEC) --parallel build $(BUILDBASE)

$(TEMPBUILDDIR)/topparallelpydep: top.F top_lattice.F top_fsl.F dtop.F util.F topparallel.F top.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) top top_lattice.F top_fsl.F dtop.F util.F topparallel.F $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/envparallelpydep: $(TEMPBUILDDIR)/topparallelpydep env.F env.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) env $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/w3dparallelpydep: $(TEMPBUILDDIR)/topparallelpydep w3d.F dw3d.F w3d_injection.F w3d_interp.F w3d_collisions.F w3d_utilities.F w3d_load.F w3dparallel.F w3d.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) --macros top.v w3d dw3d.F w3d_injection.F w3d_interp.F w3d_collisions.F w3d_load.F w3d_utilities.F w3dparallel.F $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/f3dparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep f3d.F f3d_mgrid.F f3d_ImplicitES.F f3d_mgrid_be.F f3d_bfield.F f3d_conductors.F fft.F f3d.v f3dparallel.F
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) --macros top.v f3d f3d_mgrid.F f3d_ImplicitES.F f3d_mgrid_be.F f3d_bfield.F f3d_conductors.F fft.F f3dparallel.F $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/wxyparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep wxy.F wxy.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) wxy $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/fxyparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep $(TEMPBUILDDIR)/f3dparallelpydep fxy.F fxy_mgrid.F fxy.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) fxy fxy_mgrid.F $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/wrzparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep wrz.F dwrz.F wrz.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) --macros top.v wrz dwrz.F $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/frzparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/w3dparallelpydep $(TEMPBUILDDIR)/f3dparallelpydep frz.F frz_mgrid.F90 frz_mgrid_be.F frz_ImplicitES.F frz.v frzparallel.F90
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) --compile_first frzparallel frz frz_mgrid.F90 frz_mgrid_be.F frz_ImplicitES.F frzparallel.F90 $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/herparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/wrzparallelpydep her.F her.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) her $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/cirparallelpydep: $(TEMPBUILDDIR)/topparallelpydep cir.F cir.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) cir $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/choparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/f3dparallelpydep cho.F cho.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) cho $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/em2dparallelpydep: $(TEMPBUILDDIR)/topparallelpydep em2d.F90 em2d_apml.F90 em2d_apml_cummer.F90 em2d_maxwell.F90 em2d.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) em2d em2d_apml.F90 em2d_apml_cummer.F90 em2d_maxwell.F90 $(INSTALLOPTIONS)
	touch $@

$(TEMPBUILDDIR)/em3dparallelpydep: $(TEMPBUILDDIR)/topparallelpydep $(TEMPBUILDDIR)/frzparallelpydep em3d.F90 em3d_maxwell.F90 em3d.v
	$(FORTHON) -a --pkgsuffix parallel --builddir $(TEMPBUILDDIR) $(INSTALLARGS) $(VERBOSE) $(FCOMP) $(FCOMPEXEC) $(FARGS) $(DEBUG) $(MPIPARALLEL) em3d em3d_maxwell.F90 $(INSTALLOPTIONS)
	touch $@

ranffortran.c: ranffortran.m
	$(PYTHON) -c "from Forthon.preprocess import main;main()" ranffortran.m ranffortran.c

clean:
	rm -rf $(BUILDBASEDIR) dist warp.egg-info *.o ../scripts/__version__.py

